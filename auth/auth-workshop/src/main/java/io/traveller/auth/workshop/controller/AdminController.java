package io.traveller.auth.workshop.controller;

import io.traveller.apiGateway.client.annotation.RouteURL;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理员Controller
 *
 * Created by yunai on 16/9/21.
 */
@RestController
@RequestMapping("/admin")
@RouteURL
public class AdminController {

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RouteURL
    public String add() {
        return "add";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @RouteURL
    public String update() {
        return "update";
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @RouteURL
    public String info() {
        return "info";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @RouteURL
    public String list() {
        return "list";
    }

}