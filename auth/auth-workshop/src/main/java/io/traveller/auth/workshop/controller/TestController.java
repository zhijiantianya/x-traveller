package io.traveller.auth.workshop.controller;

import io.traveller.apiGateway.client.annotation.RouteURL;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * 测试Controller
 *
 * Created by yunai on 16/9/21.
 */
@Controller
@RequestMapping("/")
@RouteURL
public class TestController {

    @ResponseBody
    @RequestMapping("/")
    @RouteURL
    public String index() {
        return "首页";
    }

    @ResponseBody
    @RequestMapping("/login")
    @RouteURL
    public String login() {
        return "登录页";
    }

    @RequestMapping("/test/go")
    @RouteURL
    public String go(HttpServletRequest request) {
        String path = request.getContextPath();
//        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        return "redirect:" + "/login";
    }

    @RequestMapping("/test/go2")
    @RouteURL
    public RedirectView go2(HttpServletRequest request) {
        if (true) {
            return new RedirectView("/login", true);
        }
        String path = request.getContextPath();
        return new RedirectView(path + "/login");
    }

}