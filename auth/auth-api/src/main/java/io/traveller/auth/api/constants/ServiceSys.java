package io.traveller.auth.api.constants;

/**
 * 服务系统
 *
 * Created by yunai on 16/9/20.
 */
public class ServiceSys {

    /**
     * 用户 - App/Wap
     */
    public static final Integer USER_APP_WAP = 1;
    /**
     * 医生 - App/Wap
     */
    public static final Integer DOCTOR_APP_WAP = 2;
    /**
     * 管理员 - Workshop
     */
    public static final Integer ADMIN_WORKSHOP = 3;
    /**
     * 官网
     */
    public static final Integer OFFICIAL_WEBSITE = 4;
    /**
     * 医生 - Workshop
     */
    public static final Integer DOCTOR_WORKSHOP = 5;

}