package io.traveller.auth.api.constants;

/**
 * 账号系统
 *
 * Created by yunai on 16/9/20.
 */
public class AccountSys {

    /**
     * 用户
     */
    public static final Integer USER = 1;
    /**
     * 医生
     */
    public static final Integer DOCTOR = 2;
    /**
     * 管理员
     */
    public static final Integer ADMIN = 3;

}
