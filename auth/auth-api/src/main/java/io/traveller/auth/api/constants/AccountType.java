package io.traveller.auth.api.constants;

/**
 * 账号类型
 *
 * Created by yunai on 16/9/18.
 */
public class AccountType {

    /**
     * 用户
     */
    public static final Integer USER = 1;
    /**
     * 游客
     */
    public static final Integer VISITOR = 2;
}
