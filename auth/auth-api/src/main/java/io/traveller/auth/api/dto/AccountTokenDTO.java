package io.traveller.auth.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 账号Token DTO
 * <p>
 * Created by yunai on 16/9/18.
 */
public class AccountTokenDTO implements Serializable {

    /**
     * accessToken
     */
    private String accessToken;
    /**
     * 账号编号
     */
    private Long accountId;
    /**
     * 账号类型
     */
    private Integer accountType;
    /**
     * 账号状态
     */
    private Integer accountStatus;
    /**
     * 用户编号
     */
    private Long userId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 过期时间
     */
    private Date expireTime;
    /**
     * 剩余秒数
     */
    private Integer expireSeconds;

    public AccountTokenDTO(String accessToken, Long accountId, Integer accountType, Integer accountStatus, Long userId, Integer expireSeconds) {
        this.accessToken = accessToken;
        this.accountId = accountId;
        this.accountType = accountType;
        this.accountStatus = accountStatus;
        this.userId = userId;
        this.expireSeconds = expireSeconds;
    }

    public Long getAccountId() {
        return accountId;
    }

    public AccountTokenDTO setAccountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public AccountTokenDTO setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public AccountTokenDTO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public AccountTokenDTO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public AccountTokenDTO setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public Integer getExpireSeconds() {
        return expireSeconds;
    }

    public AccountTokenDTO setExpireSeconds(Integer expireSeconds) {
        this.expireSeconds = expireSeconds;
        return this;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public AccountTokenDTO setAccountType(Integer accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public AccountTokenDTO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }
}