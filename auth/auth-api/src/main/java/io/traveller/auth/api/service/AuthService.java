package io.traveller.auth.api.service;

import io.traveller.auth.api.dto.AccountTokenDTO;

/**
 * 权限Service接口
 * <p>
 * Created by yunai on 16/9/18.
 */
public interface AuthService {

    // TODO 认证后，返回code

    AccountTokenDTO authorize(Integer accountSys, String username, String password);

    AccountTokenDTO authorize(Integer accountSys, String refreshToken);

    AccountTokenDTO authorize0(Integer accountSys, String authorizationCode);

    AccountTokenDTO authorize0(Integer accountSys, String clientId, String clientSecret);

    AccountTokenDTO findAccountToken(Integer accountSys, String accessToken);

    boolean hasPermission(Long accountId, Integer accountType, Integer serviceSys,
                          String requestURI, String requestMethod);

}