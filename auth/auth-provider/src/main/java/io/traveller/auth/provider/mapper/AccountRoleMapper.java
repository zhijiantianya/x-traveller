package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.AccountRole;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * {@link AccountRole} Mapper
 *
 * Created by yunai on 16/9/20.
 */
@MyBatisMapper
public interface AccountRoleMapper {

    List<AccountRole> selectAccountRolesByAccountIdAndServiceSys(
            @Param("accountId") Long accountId,
            @Param("serviceSys") Integer serviceSys,
            @Param("fields") Field field);

}
