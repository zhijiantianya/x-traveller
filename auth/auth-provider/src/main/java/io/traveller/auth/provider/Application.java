package io.traveller.auth.provider;

import com.alibaba.dubbo.container.Main;

import java.io.IOException;

/**
 * 认证Provider Application
 *
 * Created by yunai on 16/9/18.
 */
public class Application {

    public static void main(String[] args) throws IOException {
        Main.main(args);
    }

}
