package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.AccountToken;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

/**
 * {@link AccountToken} Mapper
 *
 * Created by yunai on 16/9/18.
 */
@MyBatisMapper
public interface AccountTokenMapper {

    void insertAccountToken(AccountToken accountToken);

    AccountToken selectAccountTokenByAccessTokenAndSys(
            @Param("accessToken") String accessToken,
            @Param("sys") Integer sys,
            @Param("fields") Field field
    );

}