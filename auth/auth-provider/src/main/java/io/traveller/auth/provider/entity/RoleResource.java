package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 角色资源关联表
 *
 * Created by yunai on 16/9/20.
 */
public class RoleResource {

    /**
     * 编号
     */
    private Long id;
    /**
     * 角色编号
     */
    private Long roleId;
    /**
     * 资源编号
     */
    private Long resourceId;
    /**
     * 服务系统
     */
    private Integer serviceSys;
    /**
     * 添加时间
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public RoleResource setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getRoleId() {
        return roleId;
    }

    public RoleResource setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public RoleResource setResourceId(Long resourceId) {
        this.resourceId = resourceId;
        return this;
    }

    public Integer getServiceSys() {
        return serviceSys;
    }

    public RoleResource setServiceSys(Integer serviceSys) {
        this.serviceSys = serviceSys;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RoleResource setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }
}
