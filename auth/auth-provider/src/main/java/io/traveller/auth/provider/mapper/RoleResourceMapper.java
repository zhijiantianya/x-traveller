package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.RoleResource;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * {@link RoleResource} Mapper
 *
 * Created by yunai on 16/9/20.
 */
@MyBatisMapper
public interface RoleResourceMapper {

    List<RoleResource> selectRoleResources(@Param("fields") Field field);

}
