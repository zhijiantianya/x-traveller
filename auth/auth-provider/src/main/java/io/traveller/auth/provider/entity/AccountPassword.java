package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 账号密码表
 *
 * Created by yunai on 16/9/18.
 */
public class AccountPassword {

    /**
     * 状态 - 开启
     */
    public static final Integer STATUS_ENABLE = 1;
    /**
     * 状态 - 禁用
     */
    public static final Integer STATUS_DISABLE = 2;

    /**
     * 账号密码表编号
     */
    private Long id;
    /**
     * 账号编号
     */
    private Long accountId;
    /**
     * 所属账号系统
     */
    private Integer sys;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getSys() {
        return sys;
    }

    public void setSys(Integer sys) {
        this.sys = sys;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
