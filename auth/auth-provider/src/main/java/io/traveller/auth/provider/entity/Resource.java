package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 资源
 *
 * Created by yunai on 16/9/20.
 */
// TODO identify + serviceSys 唯一索引
public class Resource {

    /**
     * 编号 - 根节点
     */
    public static final Long ID_ROOT = 0L;

    /**
     * 状态 - 开启
     */
    public static final Integer STATUS_ENABLE = 1;
    /**
     * 状态 - 禁用
     */
    public static final Integer STATUS_DISABLE = 2;

    /**
     * 类型 - 菜单
     */
    public static final Integer TYPE_MENU = 1;
    /**
     * 类型 - 操作
     */
    public static final Integer TYPE_OPERATION = 2;

    /**
     * 方法 - 全部
     */
    public static final String METHOD_ALL = "*";

    /**
     * 资源编号
     */
    private Long id;
    /**
     * 名字
     */
    private String name;
    /**
     * 标识
     */
    private String identify;
    /**
     * 描述
     */
    private String description;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 服务系统
     */
    private Integer serviceSys;
    /**
     * 添加时间
     */
    private Date createTime;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 父级资源编号
     */
    private Long pid;
    /**
     * url
     */
    private String url;
    /**
     * url对应的method
     */
    private String method;

    public Long getId() {
        return id;
    }

    public Resource setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Resource setName(String name) {
        this.name = name;
        return this;
    }

    public String getIdentify() {
        return identify;
    }

    public Resource setIdentify(String identify) {
        this.identify = identify;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Resource setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public Resource setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Integer getServiceSys() {
        return serviceSys;
    }

    public Resource setServiceSys(Integer serviceSys) {
        this.serviceSys = serviceSys;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Resource setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public Resource setType(Integer type) {
        this.type = type;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public Resource setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Resource setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public Resource setMethod(String method) {
        this.method = method;
        return this;
    }

}
