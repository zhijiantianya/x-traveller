package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 账号角色关联表
 *
 * Created by yunai on 16/9/20.
 */
public class AccountRole {

    /**
     * 编号
     */
    private Long id;
    /**
     * 账号编号
     */
    private Long accountId;
    /**
     * 角色编号
     */
    private Long roleId;
    /**
     * 服务系统
     */
    private Integer serviceSys;
    /**
     * 添加时间
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public AccountRole setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getAccountId() {
        return accountId;
    }

    public AccountRole setAccountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public Long getRoleId() {
        return roleId;
    }

    public AccountRole setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    public Integer getServiceSys() {
        return serviceSys;
    }

    public AccountRole setServiceSys(Integer serviceSys) {
        this.serviceSys = serviceSys;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public AccountRole setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }
}