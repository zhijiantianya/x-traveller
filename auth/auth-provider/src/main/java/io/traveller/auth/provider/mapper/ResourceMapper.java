package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.Resource;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * {@link Resource} Mapper
 *
 * Created by yunai on 16/9/20.
 */
@MyBatisMapper
public interface ResourceMapper {

    List<Resource> selectResourcesByTypesAndStatus(
            @Param("types") Collection<Integer> types,
            @Param("status") Integer status,
            @Param("fields") Field field
    );

}