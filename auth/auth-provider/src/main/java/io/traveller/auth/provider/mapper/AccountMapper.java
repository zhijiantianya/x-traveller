package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.Account;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

/**
 * {@link Account} Mapper
 *
 * Created by yunai on 16/9/18.
 */
@MyBatisMapper
public interface AccountMapper {

    Account selectAccount(
            @Param("id") Long id,
            @Param("fields") Field field
    );

}