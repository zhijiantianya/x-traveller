package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.AccountPassword;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

/**
 * {@link AccountPassword} Mapper
 *
 * Created by yunai on 16/9/18.
 */
@MyBatisMapper
public interface AccountPasswordMapper {

    AccountPassword selectAccountPasswordBySysAndUsernameAndPassword(
            @Param("sys") Integer sys,
            @Param("username") String username,
            @Param("password") String password,
            @Param("fields") Field field
    );


}
