package io.traveller.auth.provider.mapper;

import io.traveller.auth.provider.entity.Role;
import io.traveller.common.annotation.MyBatisMapper;
import io.traveller.common.mybatis.Field;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * {@link Role} Mapper
 *
 * Created by yunai on 16/9/20.
 */
@MyBatisMapper
public interface RoleMapper {

    List<Role> selectRolesByStatus(
            @Param("status") Integer status,
            @Param("fields") Field field
    );

}