package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 账号认证Token
 *
 * Created by yunai on 16/9/18.
 */
public class AccountToken {

    /**
     * 状态 - 开启
     */
    public static final Integer STATUS_ENABLE = 1;
    /**
     * 状态 - 无效
     */
    public static final Integer STATUS_DISABLE = 2;

    /**
     * 账号密码表编号
     */
    private Long id;
    /**
     * 账号编号
     */
    private Long accountId;
    /**
     * 所属账号系统
     */
    private Integer sys;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 认证Token
     */
    private String accessToken;
    /**
     * 刷新Token
     */
    private String refreshToken;
    /**
     * 过期时间
     */
    private Date expireTime;

    public AccountToken() {
    }

    public AccountToken(Long accountId, Integer sys, Date createTime, Integer status, String accessToken, String refreshToken, Date expireTime) {
        this.accountId = accountId;
        this.sys = sys;
        this.createTime = createTime;
        this.status = status;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expireTime = expireTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getSys() {
        return sys;
    }

    public void setSys(Integer sys) {
        this.sys = sys;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
