package io.traveller.auth.provider.entity;

import com.sun.org.apache.xpath.internal.operations.String;

import java.util.Date;

/**
 * 角色
 *
 * Created by yunai on 16/9/20.
 */
// TODO identify + serviceSys 唯一索引
public class Role {

    /**
     * 状态 - 开启
     */
    public static final Integer STATUS_ENABLE = 1;
    /**
     * 状态 - 禁用
     */
    public static final Integer STATUS_DISABLE = 2;

    /**
     * 角色编号
     */
    private Long id;
    /**
     * 角色名
     */
    private String name;
    /**
     * 标识
     */
    private String identify;
    /**
     * 描述
     */
    private String description;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 服务系统
     */
    private Integer serviceSys;
    /**
     * 添加时间
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public Role setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public String getIdentify() {
        return identify;
    }

    public Role setIdentify(String identify) {
        this.identify = identify;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public Role setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Integer getServiceSys() {
        return serviceSys;
    }

    public Role setServiceSys(Integer serviceSys) {
        this.serviceSys = serviceSys;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Role setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Role setDescription(String description) {
        this.description = description;
        return this;
    }
}
