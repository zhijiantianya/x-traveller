package io.traveller.auth.provider.entity;

import java.util.Date;

/**
 * 账号
 *
 * Created by yunai on 16/9/18.
 */
public class Account {

    /**
     * 账号编号
     */
    private Long id;
    /**
     * 所属账号系统
     */
    private Integer sys;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 用户编号
     */
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSys() {
        return sys;
    }

    public void setSys(Integer sys) {
        this.sys = sys;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
