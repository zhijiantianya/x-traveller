package io.traveller.auth.provider.service;

import io.traveller.auth.api.constants.AccountSys;
import io.traveller.auth.api.dto.AccountTokenDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

/**
 * {@link AuthServiceImpl} 测试类
 *
 * Created by yunai on 16/9/21.
 */
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/application-context-dubbo.xml",
        "classpath*:/META-INF/spring/application-context-mysql.xml"})
public class AuthServiceImplTest extends AbstractTransactionalTestNGSpringContextTests {

    //        AccountTokenDTO tokenDTO = authService.findAccountToken(1, "eceb05335c9940dbaa9ca379b8840976");
//        Assert.notNull(tokenDTO, "tokenDTO 不能为空");
//    AuthService authService = ctx.getBean(AuthServiceImpl.class);
//        authService.hasPermission(3L, 3, 3, "admin/list2.json", "GET");

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthServiceImpl authService;

    /**
     * {@link AuthServiceImpl#authorize(Integer, String, String)}登陆成功
     */
    @Test
    public void testAuthorize() {
        AccountTokenDTO dto = authService.authorize(1, "yunai", "buzhidao");
        assertNotNull(dto.getAccountId(), "accountId 不能为空.");
    }

    /**
     * {@link AuthServiceImpl#authorize(Integer, String, String)}登陆成功输出调试信息
     */
    @Test
    @Rollback(false)
    public void testAuthorizeForDebug() {
        AccountTokenDTO dto = authService.authorize(AccountSys.ADMIN, "admin_root", "admin_root");
        logger.info("[testAuthorizeForDebug][username({}) access_token({})]", "admin_root", dto.getAccessToken());
    }

}