package io.traveller.home.webapp;

import io.traveller.apiGateway.client.EnableRouteClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * home-webapp 启动器
 *
 * Created by yunai on 16/9/28.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableRouteClient
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}