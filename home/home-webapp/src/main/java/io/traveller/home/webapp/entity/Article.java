package io.traveller.home.webapp.entity;

/**
 * 文章
 *
 * Created by yunai on 16/9/28.
 */
public class Article {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 分类编号
     */
    private Integer cid;
    /**
     * url路径
     */
    private String path;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;

    public Article(Integer id, Integer cid, String path, String title, String content) {
        this.id = id;
        this.cid = cid;
        this.path = path;
        this.title = title;
        this.content = content;
    }

    public Integer getCid() {
        return cid;
    }

    public Article setCid(Integer cid) {
        this.cid = cid;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Article setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getPath() {
        return path;
    }

    public Article setPath(String path) {
        this.path = path;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Article setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Article setContent(String content) {
        this.content = content;
        return this;
    }
}