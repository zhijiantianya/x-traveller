package io.traveller.home.webapp.entity;

/**
 * 分类
 *
 * Created by yunai on 16/9/28.
 */
public class Category {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 名字
     */
    private String name;

    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public Category setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }
}
