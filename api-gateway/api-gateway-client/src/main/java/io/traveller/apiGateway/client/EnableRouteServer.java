package io.traveller.apiGateway.client;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启Route服务端注解
 *
 * Created by yunai on 16/9/28.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(RouteServerConfiguration.class)
public @interface EnableRouteServer {
}