package io.traveller.apiGateway.client.annotation;

import io.traveller.apiGateway.client.constants.SecurityLevel;

import java.lang.annotation.*;

/**
 * 路由URL注解
 *
 * Created by yunai on 16/9/27.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RouteURL {

    SecurityLevel[] securityLevel() default {};

}
