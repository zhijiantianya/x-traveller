package io.traveller.apiGateway.client;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启Route客户端注解
 *
 * Created by yunai on 16/9/28.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(RouteClientConfiguration.class)
public @interface EnableRouteClient {
}