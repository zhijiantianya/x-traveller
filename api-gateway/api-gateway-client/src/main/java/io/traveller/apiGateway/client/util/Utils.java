package io.traveller.apiGateway.client.util;

import io.traveller.apiGateway.client.route.RouteExt;
import io.traveller.apiGateway.client.route.RouteURLExt;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.regex.Pattern;

/**
 * 工具类
 *
 * Created by yunai on 16/9/28.
 */
public class Utils {

    private static final Pattern PATTERN_STATIC_RESOURCE = Pattern.compile("\\.+[html|css|js|png|ico]+(\\?.*)?$");

    /**
     * 格式化URL
     *  1. 将//变成/. 比如 user//info => user/info
     *  2. 去掉结尾的/. 比如 user/add// => user/add
     *
     * @param url URL
     * @return URL
     */
    public static String formatURL(String url) {
        url = url.replaceAll("/+", "/");
        if (url.length() > 1 && url.endsWith("/")) {
            url = org.apache.commons.lang3.StringUtils.substringBeforeLast(url, "/");
        }
        return url;
    }

    /**
     * 校验RouteExt格式
     *
     * @param route 路由信息
     * @throws RuntimeException 当不合法时，抛出异常
     */
    public static void validRouteExt(RouteExt route) {
        Assert.isTrue(StringUtils.hasText(route.getServiceId()), "route.serverId 不能为空");
        Assert.notNull(route.getServiceSys(), "route.serviceSys 不能为空");
//        Assert.notNull(route.getAccountSys(), "route.accountSys 不能为空"); 例如官网，无账号体系
        Assert.isTrue(StringUtils.hasText(route.getContextPath()), "route.contextPath 不能为空");
        Assert.notNull(route.getSecurityLevelDefault(), "route.securityLevelDefault 不能为空");
        for (RouteURLExt url : route.getUrls()) {
            Assert.isTrue(StringUtils.hasText(url.getUrl()), "route.url.url 不能为空");
            Assert.isTrue(StringUtils.hasText(url.getMethod()), "route.url.method 不能为空");
            RequestMethod method = RequestMethod.valueOf(url.getMethod());
            Assert.notNull(method, String.format("route.url.method 值不合法 (%s)", url.getMethod()));
            Assert.notNull(url.getSecurityLevel(), "route.url.securityLevel 不能为空");
        }
    }

    /**
     * 返回FullServiceId
     *
     * @param serviceId serviceId
     * @return FullServiceId
     */
    public static String getFullServiceId(String serviceId) {
        return String.format("/services/%s", serviceId);
    }

//    /**
//     * 根据url判断是否为静态资源
//     *
//     * @param url URL
//     * @return 是否静态资源
//     */
//    public static boolean isStaticResource(String url) {
//        if (!StringUtils.hasText(url)) {
//            return false;
//        }
//        return
//    }

    public static void main(String[] args) {

    }

}
