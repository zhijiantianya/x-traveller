package io.traveller.apiGateway.client.route;

import io.traveller.apiGateway.client.constants.SecurityLevel;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link org.springframework.cloud.netflix.zuul.filters.Route}增加拓展字段
 *
 * Created by yunai on 16/9/12.
 */
@SuppressWarnings("JavadocReference")
public class RouteExt {

    /**
     * 服务编号
     */
    private String serviceId;
    /**
     * 服务系统
     */
    private Integer serviceSys;
    /**
     * 账号系统
     */
    private Integer accountSys;
    /**
     * 服务器contextPath
     */
    private String contextPath;
    /**
     * 是否自动生成游客
     */
    private boolean autoGenVisitor = false;
    /**
     * 默认安全级别。
     * 若URL未配置安全级别, 则使用默认安全级别
     */
    private SecurityLevel securityLevelDefault;
    /**
     * URL规则List
     */
    private List<RouteURLExt> urls;

    public RouteExt() {
    }

    public RouteExt(String serviceId, Integer serviceSys, Integer accountSys, String contextPath, boolean autoGenVisitor,
                    SecurityLevel securityLevelDefault) {
        this.serviceId = serviceId;
        this.serviceSys = serviceSys;
        this.accountSys = accountSys;
        this.contextPath = contextPath;
        this.autoGenVisitor = autoGenVisitor;
        this.securityLevelDefault = securityLevelDefault;
        this.urls = new ArrayList<>();
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getAccountSys() {
        return accountSys;
    }

    public void setAccountSys(Integer accountSys) {
        this.accountSys = accountSys;
    }

    public boolean isAutoGenVisitor() {
        return autoGenVisitor;
    }

    public void setAutoGenVisitor(boolean autoGenVisitor) {
        this.autoGenVisitor = autoGenVisitor;
    }

    public SecurityLevel getSecurityLevelDefault() {
        return securityLevelDefault;
    }

    public RouteExt setSecurityLevelDefault(SecurityLevel securityLevelDefault) {
        this.securityLevelDefault = securityLevelDefault;
        return this;
    }

    public Integer getServiceSys() {
        return serviceSys;
    }

    public RouteExt setServiceSys(Integer serviceSys) {
        this.serviceSys = serviceSys;
        return this;
    }

    public List<RouteURLExt> getUrls() {
        return urls;
    }

    public RouteExt setUrls(List<RouteURLExt> urls) {
        this.urls = urls;
        return this;
    }

    public String getContextPath() {
        return contextPath;
    }

    public RouteExt setContextPath(String contextPath) {
        this.contextPath = contextPath;
        return this;
    }
}