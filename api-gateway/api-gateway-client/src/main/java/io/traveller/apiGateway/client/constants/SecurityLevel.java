package io.traveller.apiGateway.client.constants;

/**
 * URL 安全级别
 *
 * Created by yunai on 16/9/27.
 */
public enum SecurityLevel {

    NONE,
    USER_LOGIN,
    AUTH;

}
