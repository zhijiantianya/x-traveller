package io.traveller.apiGateway.client.route;

import com.alibaba.fastjson.annotation.JSONField;
import io.traveller.apiGateway.client.constants.SecurityLevel;

/**
 * {@link org.springframework.cloud.netflix.zuul.filters.Route}每个URL做拓展
 *
 * Created by yunai on 16/9/12.
 */
@SuppressWarnings("JavadocReference")
public class RouteURLExt {

    /**
     * 路由Ext
     */
    @JSONField(serialize = false, deserialize = false)
    private RouteExt routeExt;
    /**
     * URL
     */
    private String url;
    /**
     * Method
     */
    private String method;
    /**
     * 安全等级
     */
    private SecurityLevel securityLevel;
    // TODO 暂未实现
    /**
     * 是否获取三方平台openid
     * 该标识主要用于三方平台浏览器, 即使不登陆, 也可以获取三方平台openid。适用于如下平台:
     *  1. 微信浏览器
     * 要求处于未登陆的情况下。
     */
    private Boolean enableThirdPlatformOpenid;
    /**
     * 是否登陆拦截时, 使用三方平台的登陆拦截。适用于如下平台:
     *  1. 微信平台
     *  2. 支付宝平台
     * 要求securityLevel为要求登陆时
     */
    private Boolean enableThirdPlatformLogin;

    public RouteURLExt() {
    }

    public RouteURLExt(String url, String method, SecurityLevel securityLevel,
                       Boolean enableThirdPlatformOpenid, Boolean enableThirdPlatformLogin) {
        this.url = url;
        this.method = method;
        this.securityLevel = securityLevel;
        this.enableThirdPlatformOpenid = enableThirdPlatformOpenid;
        this.enableThirdPlatformLogin = enableThirdPlatformLogin;
    }

    public RouteExt getRouteExt() {
        return routeExt;
    }

    public void setRouteExt(RouteExt routeExt) {
        this.routeExt = routeExt;
    }

    public SecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public RouteURLExt setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public RouteURLExt setMethod(String method) {
        this.method = method;
        return this;
    }

    public Boolean getEnableThirdPlatformOpenid() {
        return enableThirdPlatformOpenid;
    }

    public RouteURLExt setEnableThirdPlatformOpenid(Boolean enableThirdPlatformOpenid) {
        this.enableThirdPlatformOpenid = enableThirdPlatformOpenid;
        return this;
    }

    public Boolean getEnableThirdPlatformLogin() {
        return enableThirdPlatformLogin;
    }

    public RouteURLExt setEnableThirdPlatformLogin(Boolean enableThirdPlatformLogin) {
        this.enableThirdPlatformLogin = enableThirdPlatformLogin;
        return this;
    }
}