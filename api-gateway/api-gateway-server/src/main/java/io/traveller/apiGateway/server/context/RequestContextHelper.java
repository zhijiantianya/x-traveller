package io.traveller.apiGateway.server.context;

import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.util.HTTPRequestUtils;
import io.traveller.apiGateway.client.constants.SecurityLevel;

import java.util.List;

/**
 * {@link com.netflix.zuul.context.RequestContext}的工具类。
 * 用于添加自定义字段, 保证可读性
 *
 * Created by yunai on 16/9/12.
 */
public class RequestContextHelper {

    public static String getRequestParameter(String key) {
        List<String> values = HTTPRequestUtils.getInstance().getQueryParams().get(key);
        if (values != null && !values.isEmpty()) {
            return values.get(0);
        }
        return null;
    }

    public static void setAutoGenVisitor(RequestContext ctx, boolean autoGenVisitor) {
        ctx.put("autoGenVisitor", autoGenVisitor);
    }

    public static boolean isAutoGenVisitor(RequestContext ctx) {
        return (boolean) ctx.get("autoGenVisitor");
    }

    public static void setSecurityLevel(RequestContext ctx, SecurityLevel securityLevel) {
        ctx.put("securityLevel", securityLevel);
    }

    public static SecurityLevel getSecurityLevel(RequestContext ctx) {
        return (SecurityLevel) ctx.get("securityLevel");
    }

    public static void setAccountSys(RequestContext ctx, Integer accountSys) {
        if (accountSys == null) {
            return;
        }
        ctx.put("accountSys", accountSys);
    }

    public static Integer getAccountSys() {
        return getAccountSys(RequestContext.getCurrentContext());
    }

    public static Integer getAccountSys(RequestContext ctx) {
        return (Integer) ctx.get("accountSys");
    }

    public static Integer getServiceSys(RequestContext ctx) {
        return (Integer) ctx.get("serviceSys");
    }

    public static void setServiceSys(RequestContext ctx, Integer serviceSys) {
        ctx.put("serviceSys", serviceSys);
    }

    // =========== 其他逻辑产生的ctx的value的操作 ===========

    public static String getServiceId(RequestContext ctx) {
        return (String) ctx.get("serviceId");
    }

    public static void removeServiceId(RequestContext ctx) {
        ctx.remove("serviceId");
    }

    public static String getRequestURI(RequestContext ctx) {
        return (String) ctx.get("requestURI");
    }

    public static String getRequestMethod(RequestContext ctx) {
        return ctx.getRequest().getMethod();
    }

    // =========== 账号相关 ==========

    public static Long getAccountId(RequestContext ctx) {
        return (Long) ctx.get("accountId");
    }

    public static void setAccountId(RequestContext ctx, Long accountId) {
        ctx.put("accountId", accountId);
    }

    public static Integer getAccountType(RequestContext ctx) {
        return (Integer) ctx.get("accountType");
    }

    public static void setAccountType(RequestContext ctx, Integer accountType) {
        ctx.put("accountType", accountType);
    }

    public static Long getUserIdId(RequestContext ctx) {
        return (Long) ctx.get("userIdId");
    }

    public static void setUserIdId(RequestContext ctx, Long userIdId) {
        ctx.put("userIdId", userIdId);
    }

    // =========== 公用逻辑 ==========

    // TODO 需要重构
    public static void setResponse(RequestContext ctx, int code, String msg) {
        // 响应请求
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        obj.put("_msg", msg);
        ctx.setResponseBody(obj.toString());
        ctx.getResponse().setCharacterEncoding("UTF-8");
        ctx.getResponse().setHeader("Content-type", "text/html;charset=UTF-8");
        // 避免RibbonRoutingFilter进行route
        removeServiceId(ctx);
    }

}
