package io.traveller.apiGateway.server.filter;

/**
 * 过滤器类型
 *
 * Created by yunai on 16/9/6.
 */
public class FilterType {

    /**
     * 前置
     */
    public static final String PRE = "pre";
    /**
     * 后置
     */
    public static final String POST = "post";
    /**
     * 路由
     */
    public static final String ROUTE = "route";

}