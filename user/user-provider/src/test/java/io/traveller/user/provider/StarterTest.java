package io.traveller.user.provider;

import java.io.IOException;

/**
 * {@link Starter}测试类
 *
 * Created by yunai on 16/9/3.
 */
public class StarterTest {

    public static void main(String[] args) throws IOException {
        Starter.main(args);
    }

}