package io.traveller.user.provider;

import com.alibaba.dubbo.container.Main;

import java.io.IOException;

/**
 * 启动器
 *
 * Created by yunai on 16/9/3.
 */
public class Starter {

    public static void main(String[] args) throws IOException {
        Main.main(args);
    }

}