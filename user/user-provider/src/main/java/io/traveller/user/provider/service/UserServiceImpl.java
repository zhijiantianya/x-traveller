package io.traveller.user.provider.service;

import io.traveller.user.api.service.UserService;

/**
 * {@link UserService}实现类
 * Created by yunai on 14/10/23.
 */
public class UserServiceImpl implements UserService {

    @Override
    public String login(String username, String password) {
        String log = String.format("[login][username(%s) password(%s)]", username, password);
        System.out.println(log);
        return log;
    }

}