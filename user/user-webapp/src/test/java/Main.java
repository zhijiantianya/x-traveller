import io.traveller.user.api.service.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main
 *
 * Created by yunai on 16/9/4.
 */
public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath:/spring/application-context-dubbo.xml");
        String log = context.getBean(UserService.class).login("ha", "ww");
        System.out.println("哈哈哈: " + log);
    }

}
