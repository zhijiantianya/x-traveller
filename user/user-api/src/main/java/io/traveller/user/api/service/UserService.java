package io.traveller.user.api.service;

/**
 * 用户模块Service接口
 * Created by yunai on 14/10/23.
 */
public interface UserService {

    String login(String username,
                 String password);

}