package io.traveller.common.annotation;

import java.lang.annotation.*;

/**
 * Mybatis Mapper注解
 * Created by yunai on 16/9/18.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface MyBatisMapper {
}