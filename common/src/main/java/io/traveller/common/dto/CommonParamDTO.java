package io.traveller.common.dto;

/**
 * 公用参数DTO
 *
 * Created by yunai on 16/9/18.
 */
public class CommonParamDTO {

    /**
     * IP
     */
    private String ip;
    /**
     * 终端
     */
    private String ua;
    /**
     * 终端
     */
    private Integer terminal;
    /**
     * 终端版本
     */
    private Integer version;
    /**
     * 终端系统
     */
    private String sdk;
    /**
     * 终端udid
     */
    private String udid;

    public String getIp() {
        return ip;
    }

    public CommonParamDTO setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public String getUa() {
        return ua;
    }

    public CommonParamDTO setUa(String ua) {
        this.ua = ua;
        return this;
    }

    public Integer getTerminal() {
        return terminal;
    }

    public void setTerminal(Integer terminal) {
        this.terminal = terminal;
    }

    public Integer getVersion() {
        return version;
    }

    public CommonParamDTO setVersion(Integer version) {
        this.version = version;
        return this;
    }

    public String getSdk() {
        return sdk;
    }

    public CommonParamDTO setSdk(String sdk) {
        this.sdk = sdk;
        return this;
    }

    public String getUdid() {
        return udid;
    }

    public CommonParamDTO setUdid(String udid) {
        this.udid = udid;
        return this;
    }
}